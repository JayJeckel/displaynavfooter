base   displaynavfooter
author Jay Jeckel
email  jeckelmail@gmail.com
date   2017-01-11
name   Display Navigation Footer
desc   The Display Navigation Footer Plugin ?.
url    https://www.dokuwiki.org/plugin:displaynavfooter

downloadurl  https://gitlab.com/JayJeckel/displaynavfooter/-/archive/master/displaynavfooter-master.zip
bugtracker   https://gitlab.com/JayJeckel/displaynavfooter/issues
sourcerepo   https://gitlab.com/JayJeckel/displaynavfooter/

compatible   Elenor of Tsort, Frusterick Manners, Greebo, Hogfather
depends
conflicts
similar
tags
