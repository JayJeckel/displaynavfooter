<?php
/**
 * Display Navigation Footer Plugin (Syntax Component)
 *
 * Description: The Display Navigation Footer Plugin ?.
 *
 * Syntax: ?
 *
 * @license    Creative Commons - Attribution, Non-Commercial, Share-Alike (https://creativecommons.org/licenses/by-nc-sa/4.0/)
 * @author     Jay Jeckel <jeckelmail@gmail.com>
 */

if(!defined('DOKU_INC')) { die(); }

class syntax_plugin_displaynavfooter extends DokuWiki_Syntax_Plugin
{
    const PATTERN_OLD = '{{' . 'NAVFOOTER' . '>' . '.+?' . ',' . '.+?' . '}}';
    const PATTERN = '<<' . 'display' . '\s' . 'navfooter' . '\s' . '.+?' . '\s' . '.+?' . '>>';

    function getInfo() { return confToHash(dirname(__FILE__) . '/plugin.info.txt'); }

    function getType() { return 'substition'; }

    function getPType() { return 'block'; }

    function getSort() { return 5; }

    function connectTo($mode)
    {
        $this->Lexer->addSpecialPattern(self::PATTERN_OLD, $mode, 'plugin_displaynavfooter');
        $this->Lexer->addSpecialPattern(self::PATTERN, $mode, 'plugin_displaynavfooter');
    }

    function handle($match, $state, $pos, Doku_Handler $handler)
    {
        $deprecated = $match[0] == '{';
        $match = $deprecated ? substr($match, 12, -2) : substr($match, 20, -2);
        list($left, $right) = explode(' ', $match, 2);
        if (empty($left) || $left == '-' || $left == 'null') { $left = null; }
        if (empty($right) || $right == '-' || $right == 'null') { $right = null; }
        return array($left, $right, $deprecated);
    }

    // https://unicode-table.com/en/sets/arrows-symbols/
    // &#9664; -- &#9654;
    function render($format, Doku_Renderer $renderer, $data)
    {
        global $ID;

        list($left, $right, $deprecated) = $data;

        $renderer->nocache();
        if ($format != 'xhtml') { return false; }

        $renderer->doc .= "\n<hr />";
        $renderer->doc .= "\n<div class='plugin__displaynavfooter'>";

        if ($left != null)
        {
            $link = $this->_internallink($renderer, $left);
            $link['class'][] = 'float_left';
            $renderer->doc .= "\n<a href='" . $link['url'] . "' class='" . implode(' ', $link['class']) . "' rel='" . $link['rel'] . "' target='" . $link['target'] . "'>&#11164; " . $link['name'] . "</a>";
        }

        if ($right != null)
        {
            $link = $this->_internallink($renderer, $right);
            $link['class'][] = 'float_right';
            $renderer->doc .= "\n<a href='" . $link['url'] . "' class='" . implode(' ', $link['class']) . "' rel='" . $link['rel'] . "' target='" . $link['target'] . "'>" . $link['name'] . " &#11166;</a>";
        }

        {
            $link = $this->_internallink($renderer, $ID);
            $link['class'][] = 'float_center';
            $renderer->doc .= "\n<a href='" . $link['url'] . "' class='" . implode(' ', $link['class']) . "' rel='" . $link['rel'] . "' target='" . $link['target'] . "'>" . $link['name'] . "</a>";
        }

        $renderer->doc .= "\n<div class='float_clear'></div>";

        if ($deprecated)
        {
            $renderer->doc .= "\n<div class='deprecated'>Display Navigation Footer syntax deprecated. Use &lt;&lt;display navfooter&gt;&gt; syntax instead.</div>";
        }

        $renderer->doc .= "\n</div>";
        return true;
    }

    function _internallink(Doku_Renderer &$renderer, $id, $name = null)
    {
        global $ID, $conf;

        $params = '';
        $parts  = explode('?', $id, 2);
        if (count($parts) === 2)
        {
            $id = $parts[0];
            $params = $parts[1];
        }

        if($id === '') { $id = $ID; }
        $default = $renderer->_simpleTitle($id);
        $exists = false;
        resolve_pageid(getNS($ID), $id, $exists, $renderer->date_at, true);

        $name = $renderer->_getLinkTitle($name, $default, $isImage, $id, 'content');

        @list($id, $hash) = explode('#', $id, 2);
        if (!empty($hash)) { $hash = $renderer->_headerToLink($hash); }

        if ($renderer->date_at) { $params['at'] = $renderer->date_at; }
        $link = array();
        $link['id'] = $id;
        $link['exists'] = $exists;
        $link['rel'] = ($exists ? '' : 'nofollow');
        $link['target'] = $conf['target']['wiki'];
        $link['url'] = wl($id, $params);
        $link['name'] = $name;
        $link['title'] = $id;
        $link['class'] = array();

        if ($exists) { $link['class'][] = 'wikilink1'; }
        else { $link['class'][] = 'wikilink2'; }

        if ($hash) { $link['url'] .= '#'.$hash; }

        return $link;
    }
}

//Setup VIM: ex: et ts=4 enc=utf-8 :
?>