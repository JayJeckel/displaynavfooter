# Display Navigation Footer Dokuwiki Plugin

[![MIT License](https://svgshare.com/i/TRb.svg)](https://opensource.org/licenses/MIT)
[![DokuWiki Plugin](https://svgshare.com/i/TSa.svg)](https://www.dokuwiki.org/dokuwiki)
[![Plugin Home](https://svgshare.com/i/TRw.svg)](https://www.dokuwiki.org/plugin:displaynavfooter)
[![Gitlab Repo](https://svgshare.com/i/TRR.svg)](https://gitlab.com/JayJeckel/displaynavfooter)
[![Gitlab Issues](https://svgshare.com/i/TSw.svg)](https://gitlab.com/JayJeckel/displaynavfooter/issues)
[![Gitlab Download](https://svgshare.com/i/TT5.svg)](https://gitlab.com/JayJeckel/displaynavfooter/-/archive/master/displaynavfooter-master.zip)

The Display Navigation Footer Plugin ?.

## Installation

Search and install the plugin using the [Extension Manager](https://www.dokuwiki.org/plugin:extension) or install directly using the latest [download url](https://gitlab.com/JayJeckel/displaynavfooter/-/archive/master/displaynavfooter-master.zip), otherwise refer to [Plugins](https://www.dokuwiki.org/plugins) on how to install plugins manually.

## Usage

The plugin offers one block element that expands into previous/next page navigation footer.

| Element |
|:-|:-|
| `<<display navfooter PREV_PAGE NEXT_PAGE>>` |

| Argument | Required | Description |
|:-|:-|:-|
| `PREV_PAGE` | yes | Arbitrary local wiki page id as would be passed to normal local wiki links. Use the `-` symbol to omit the previous page link. |
| `NEXT_PAGE` | yes | Arbitrary local wiki page id as would be passed to normal local wiki links. Use the `-` symbol to omit the next page link. |

## Configuration Settings

The plugin does not provide any [Configuration Manager](https://www.dokuwiki.org/config:manager) settings.

## Security

The plugin has no abnormal security concerns related to the provided functionality.
